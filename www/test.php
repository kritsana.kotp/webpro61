<!DOCTYPE HTML>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<script>
		function makeUser(name,age) {
			return {
				name: name,
				age: age
			};
		}

		let user = makeUser("John",30);
		alert(user.name);
	</script>
</body>
</html>